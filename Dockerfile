FROM postgres:13-alpine AS postgres
ENV POSTGRES_DB_DATABASE="postgres"
ENV POSTGRES_DB_USERNAME="surveyPostgres"
ENV POSTGRES_DB_PASS="postgres"
ENV POSTGRES_PASSWORD="postgres"
ENV POSTGRES_USER="postgres"
COPY infra/postgresql/initdb /docker-entrypoint-initdb.d

FROM jboss/keycloak AS keycloak
ENV JAVA_OPTS="-Dkeycloak.profile.feature.upload_scripts=enabled"
ENV DB_VENDOR=postgres
ENV DB_ADDR=postgres
ENV DB_PASSWORD=postgres
ENV DB_USER=postgres
ENV KEYCLOAK_USER=admin
ENV KEYCLOAK_PASSWORD=admin
ENV KEYCLOAK_DEFAULT_THEME=il3
COPY themes/il3 /opt/jboss/keycloak/themes/il3
